package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数が渡されていない場合のエラー処理
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();
		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST,branchNames, branchSales, "^[0-9]{3}", "支店")) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST,commodityNames, commoditySales, "^[A-Za-z0-9]{8}", "商品")) {
			return;
		}


		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//ファイルの判別のループ処理
		for(int i = 0; i < files.length ; i++) {

			String fileName  = files[i].getName();

			//対象がファイルであり「数字8桁.rcd」か判定
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);

		for(int i = 0; i < rcdFiles.size() - i; i++) {

			//売り上げファイルが連番でないエラー処理
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println("売り上げファイル名が連番になっていません");
				return;
			}
		}
		//売上ファイルの中身をリストに追加
		BufferedReader br = null;

		for(int i = 0; i < rcdFiles.size(); i++) {

			List<String> fileContents = new ArrayList<>();

			try{
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));

				String str;

				while((str = br.readLine()) != null){
					fileContents.add(str);
				}

				//売上ファイルリストのkey＝支店コード
				String branchcode = fileContents.get(0);

				File f = rcdFiles.get(i);
				String fileName = f.getName();

				//売上ファイルのフォーマットが異なる場合のエラー処理
				if(fileContents.size() != 3) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}
				//売上金額が数字であるかエラー処理
				if(!fileContents.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//支店コードに該当がない場合のエラー処理
				if (!branchNames.containsKey(branchcode)) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				//商品コードに該当がない場合のエラー処理
				if (!commodityNames.containsKey(fileContents.get(1))) {
					System.out.println(fileName + "の商品コードが不正です");
					return;
				}

				//Long型に変換
				long fileSale = Long.parseLong(fileContents.get(2));
				Long saleAmount = branchSales.get(branchcode) + fileSale;
				Long commoditySaleAmount = commoditySales.get(fileContents.get(1)) + fileSale;

				//売上金額が10桁を超えるエラー処理
				if ((saleAmount >= 10000000000L) || (commoditySaleAmount >= 10000000000L)) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//マップに追加
				branchSales.put(branchcode, saleAmount);
				commoditySales.put(fileContents.get(1), commoditySaleAmount);



			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT,branchNames, branchSales)) {
			return;
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT,commodityNames, commoditySales)) {
			return;
		}
	}


	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName,
			Map<String, String> names, Map<String, Long> sales, String regex, String which) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//ファイルが存在しないエラー処理
			if (!file.exists()) {
				System.out.println(which + FILE_NOT_EXIST);
				return false;
			}

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)

				String[] items = line.split(",");

				//ファイルのフォーマットが違うエラー処理
				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(which + FILE_INVALID_FORMAT);
					return false;
				}

				//マップに追加
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);


			}

		}catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return true;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
			return true;
		}


	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName,
			Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}


		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
